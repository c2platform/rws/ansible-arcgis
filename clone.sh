#!/bin/bash
export REPO_DIR=~/git/gitlab/c2
export C2_HTTPS_SSH=${C2_HTTPS_SSH:-https}
if [ -n "$GIT_USER" ] && [ -n "$GIT_MAIL" ]; then
  echo "GIT_USER: $GIT_USER"
  echo "GIT_MAIL: $GIT_MAIL"
else
  echo "You should set GIT_USER and GIT_MAIL. It is recommended to put this in ~/.bashrc"
  exit 1
fi
echo "Protocol: $C2_HTTPS_SSH"
if [ ! -d "$REPO_DIR" ] # Create REPO_DIR if not exist
then
  echo "Create REPO_DIR: $REPO_DIR"
  mkdir -p $REPO_DIR
fi
cd $REPO_DIR
clone_repo () {
  cd $REPO_DIR
  if [ ! -d "$REPO_DIR/$2" ]
  then
    git clone $1 $2
    cd $2
    git fetch origin $3
    git checkout $3
    git branch --set-upstream-to=origin/$3 $3
    git config pull.rebase false
    git pull origin $3
    git config user.name "$GIT_USER"
    git config user.email "$GIT_MAIL"
  fi
  cd $REPO_DIR/$2
  export git_url=$(git remote -v | grep origin | grep fetch | awk '{print $2}')
  echo ""
  echo "$(basename $git_url)"
  if [ "$git_url" != "$1" ]
  then
    echo "    $git_url → $1"
    git remote remove origin
    git remote add origin $1
  fi
  if [ "$C2_LINTERS" == "Y" ]
  then
    if [ -f ".ansible-lint" ]
    then
      echo "    Creating / updating $2/.git/hooks/pre-commit"
      curl -s -L https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-development/pre-commit > .git/hooks/pre-commit
      chmod 775 .git/hooks/pre-commit
    fi
  fi
  if [ "$C2_LINTERS" == "N" ]
  then
    if [ -f ".ansible-lint" ]
    then
      echo "    Removing $2/.git/hooks/pre-commit"
      rm .git/hooks/pre-commit
    fi
  fi
}
if [ "$C2_HTTPS_SSH" == "https" ]
then
  clone_repo "https://gitlab.com/c2platform/rws/ansible-gis.git" "ansible-gis" master
  clone_repo "https://gitlab.com/c2platform/ansible-collection-core.git" "ansible-dev-collections/ansible_collections/c2platform/core" master
  clone_repo "https://gitlab.com/c2platform/ansible-collection-mgmt.git" "ansible-dev-collections/ansible_collections/c2platform/mgmt" master
  clone_repo "https://gitlab.com/c2platform/ansible-collection-mw.git" "ansible-dev-collections/ansible_collections/c2platform/mw" master
  clone_repo "https://gitlab.com/c2platform/ansible-collection-test.git" "ansible-dev-collections/ansible_collections/c2platform/test" master
  clone_repo "https://gitlab.com/c2platform/ansible-collection-dev.git" "ansible-dev-collections/ansible_collections/c2platform/dev" master
  clone_repo "https://gitlab.com/c2platform/rws/ansible-collection-gis.git" "ansible-dev-collections/ansible_collections/c2platform/gis" master
  clone_repo "https://gitlab.com/c2platform/rws/ansible-collection-wincore.git" "ansible-dev-collections/ansible_collections/c2platform/wincore" master
  clone_repo "https://gitlab.com/c2platform/rws/ansible-execution-environment.git" "ansible-execution-environment" master
  clone_repo "https://gitlab.com/c2platform/website.git" "website" master
elif [ "$C2_HTTPS_SSH" == "ssh" ]
then
  clone_repo "git@gitlab.com:c2platform/rws/ansible-gis.git" "ansible-gis" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-core.git" "ansible-dev-collections/ansible_collections/c2platform/core" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-mgmt.git" "ansible-dev-collections/ansible_collections/c2platform/mgmt" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-mw.git" "ansible-dev-collections/ansible_collections/c2platform/mw" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-test.git" "ansible-dev-collections/ansible_collections/c2platform/test" master
  clone_repo "git@gitlab.com:c2platform/ansible-collection-dev.git" "ansible-dev-collections/ansible_collections/c2platform/dev" master
  clone_repo "git@gitlab.com:c2platform/rws/ansible-collection-gis.git" "ansible-dev-collections/ansible_collections/c2platform/gis" master
  clone_repo "git@gitlab.com:c2platform/rws/ansible-collection-wincore.git" "ansible-dev-collections/ansible_collections/c2platform/wincore" master
  clone_repo "git@gitlab.com:c2platform/rws/ansible-execution-environment.git" "ansible-execution-environment" master
  clone_repo "git@gitlab.com:c2platform/website.git" "website" master
fi
