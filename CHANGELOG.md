# CHANGELOG

## 0.0.1 (  )

* Added and configured ArcGIS 11.3 ( upgrade 11.1 → 11.3 ). Requires
  `c2platform.gis` `1.0.19`.
* New variables `gs_arcgis_server_license_commands` and
  `gs_arcgis_datastore_license_commands` for **ArcGIS 11.3**. File
  `SoftwareAuthorization.exe` is now in different location.
* `requirements.yml` `core` `1.0.21`
* `arcgis_portal_temp_dir`( not `arcgis_portal_install_temp_dir` )
* Configured `webproxy.c2platform.org` for all `linux` nodes ( except
  `gsd-rproxy1` of course).
* Introduced a new variable `gs_dev_tomcat_version` for the Ansible repository
  to enhance flexibility and maintainability. For more information refer to
  [Create a Simple Software Repository for Ansible | C2
  Platform](https://c2platform.org/docs/howto/rws/software/). This change also
  simplifies the play by switching to an archive repository (from
  https://dlcdn.apache.org/tomcat/tomcat-10 to
  https://archive.apache.org/dist/tomcat/tomcat-10).  This eliminates the need
  for continuous updates of the Tomcat version.
* Modified `Vagrantfile` and `Vagrantfile.yml` so that RHEL systems can be
  automatically registered and unregistered. See [Automated Red Hat Enterprise
  Linux Registration and
  Subscription](http://c2platform.org/docs/guidelines/dev/rhel).
