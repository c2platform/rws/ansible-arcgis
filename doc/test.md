# Test Status

| How-To                                          | mx             | Sprint  | Provision / Test / Doc | Comment |
|-------------------------------------------------|----------------|---------|------------------------|---------|
| Create a Simple CA Server using Ansible         | `ca-server`    | 2024-09 | OK / OK / OK           | `io3`   |
| Setting Up FME Flow with Ansible                | `fme`          | 2024-09 | OK / OK / OK           | `mpc2`  |
| Setup Ansible Control Node                      | `ansible`      | 2024-09 | Running /              | `mpc2`  |
| Tomcat SSL/TLS and Java Keystore and TrustStore | `tomcat`       | 2024-09 | OK / OK / OK           | `io3`   |
| Create a Simple Software Repository for Ansible | `ansible-repo` | 2024-09 | OK / OK / OK           | `io3`   |

Note: doc includes landing page.
