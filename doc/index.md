# RWS GIS Platform Automation Development Environment

## Reverse Proxy

* [Create the Reverse Proxy and Web Proxy | C2 Platform](https://c2platform.org/docs/howto/rws/dev-environment/setup/rproxy1/)

| Service       | Node          | Link                            |
|---------------|---------------|---------------------------------|
| Reverse Proxy | `gsd-rproxy1` |                                 |
| Forward Proxy | `gsd-rproxy1` |                                 |
| Alive Check   | `gsd-rproxy1` | https://c2platform.org/is-alive |


## ArcGIS Server and Data Store

* [Setup ArcGIS Server and Date Store using Ansible | C2 Platform](https://c2platform.org/docs/howto/rws/arcgis/server/)

| Service                               | Node            | Link                                                                 | Reverse Proxy |
|---------------------------------------|-----------------|----------------------------------------------------------------------|---------------|
| ArcGIS Server Administrator Directory | `gsd-agserver1` | https://gsd-agserver1.internal.c2platform.org:6443/arcgis/admin/     |               |
| ArcGIS Server Manager                 | `gsd-agserver1` | https://gsd-agserver1.internal.c2platform.org:6443/arcgis/manager/   |               |
| ArcGIS Data Store                     | `gsd-agserver1` | https://gsd-agserver1.internal.c2platform.org:2443/arcgis/datastore/ |               |

| Username    | Password       |
|-------------|----------------|
| `siteadmin` | `siteadmin123` |

https://1.1.8.100:6443/arcgis/rest/info/healthCheck?f=json

## ArcGIS Portal and Web Adaptor

* [Setup ArcGIS Portal and Web Adaptors using Ansible | C2 Platform](https://c2platform.org/docs/howto/rws/arcgis/portal/)

| Service                                                                                              | Node          | Reverse Proxy                                                                    | Comment                      |
|------------------------------------------------------------------------------------------------------|---------------|----------------------------------------------------------------------------------|------------------------------|
| [ArcGIS Portal API URL](https://gsd-agportal1.internal.c2platform.org:7443/arcgis/portaladmin)       | `gsd-portal1` |                                                                                  |                              |
| [ArcGIS Portal](https://gsd-agportal1.internal.c2platform.org:7443/arcgis/)                          | `gsd-portal1` |                                                                                  |                              |
| [ArcGIS Portal](https://gsd-agportal1.internal.c2platform.org/portal/home/)                          | `gsd-portal1` | [age.c2platform.org/portal/home/](https://age.c2platform.org/portal/home/)       | Via Web Adaptor              |
| [ArcGIS Web Adapter wizard](https://localhost/portal/webadaptor)                                     | `gsd-portal1` |                                                                                  |                              |
| [ArcGIS Server Administrator Directory](https://gsd-agportal1.internal.c2platform.org/server/admin/) | `gsd-portal1` | [age.c2platform.org/server/admin/](https://age.c2platform.org/server/admin/)     | Via Web Adaptor, `siteadmin` |
| [ArcGIS Server Manager](https://gsd-agportal1.internal.c2platform.org/server/manager/)               | `gsd-portal1` | [age.c2platform.org/server/manager/](https://age.c2platform.org/server/manager/) | Via Web Adaptor, `siteadmin` |


| Username      | Password         |
|---------------|------------------|
| `portaladmin` | `portaladmin123` |
| `siteadmin`   | `siteadmin123`   |


## FME

* [Setting Up FME Flow with Ansible | C2 Platform](https://c2platform.org/docs/howto/rws/arcgis/fme_flow/)

| Service    | Node            | Link                                                                   | Reverse Proxy |
|------------|-----------------|------------------------------------------------------------------------|---------------|
| AD         | `gsd-ad`        |                                                                        |               |
| PostgreSQL | `gsd-db1`       |                                                                        |               |
| Tomcat     | `gsd-fme-core`  | https://gsd-fme-core.internal.c2platform.org/                          |               |
| FME        | `gsd-fme-core`  | https://gsd-fme-core.internal.c2platform.org/fmeserver/                |               |
| JSP Test   | `gsd-fme-core`  | https://gsd-fme-core.internal.c2platform.org/helloworld/               |               |
| API Doc    | `gsd-fme-core`  | https://gsd-fme-core.internal.c2platform.org/fmerest/apidoc/v3/        |               |
| API Doc v4 | `gsd-fme-core`  | https://gsd-fme-core.internal.c2platform.org/fmeapiv4/docs/index.html  |               |
| Monitoring | `gsd-fme-core`  | https://gsd-fme-core.internal.c2platform.org/jolokia                   |               |
| Tomcat     | `gsd-fme-core2` | https://gsd-fme-core2.internal.c2platform.org/                         |               |
| FME        | `gsd-fme-core2` | https://gsd-fme-core2.internal.c2platform.org/fmeserver/               |               |
| JSP Test   | `gsd-fme-core2` | https://gsd-fme-core2.internal.c2platform.org/helloworld/              |               |
| API Doc    | `gsd-fme-core2` | https://gsd-fme-core2.internal.c2platform.org/fmerest/apidoc/v3/       |               |
| API Doc v4 | `gsd-fme-core2` | https://gsd-fme-core2.internal.c2platform.org/fmeapiv4/docs/index.html |               |
| Monitoring | `gsd-fme-core2` | https://gsd-fme-core2.internal.c2platform.org/jolokia                  |               |

## Geoweb

* [Setup Geoweb](https://c2platform.org/docs/howto/rws/arcgis/geoweb/)

| Service            | Node         | Link                                                             | Reverse Proxy                                  |
|--------------------|--------------|------------------------------------------------------------------|------------------------------------------------|
| VertiGIS Web       | `gsd-geoweb` | https://gsd-geoweb.internal.c2platform.org:4443/ModuleViewer/    | https://geoweb.c2platform.org/ModuleViewer/    |
| VertiGIS Reporting | `gsd-geoweb` | https://gsd-geoweb.internal.c2platform.org:4444/ModuleReporting/ | https://geoweb.c2platform.org/ModuleReporting/ |

Note: only the the reverse proxy links will work. The local links with port 4443
and 4444 will not allow login. ArcGIS Portal will show message the the redirect
uri is invalid. Which is correct, the reverse proxy links are configured as the
redirect URI's.

## CA Server

* [Create a Simple CA Server using Ansible | C2 Platform](https://c2platform.org/docs/howto/rws/ca/)

| Service                        | Node                      | Link                                          | Reverse Proxy |
|--------------------------------|---------------------------|-----------------------------------------------|---------------|
| File share for certificates    | `gsd-ansible-file-share1` |                                               |               |
| Simple Ansible based CA Server | `gsd-ca-server`           |                                               |               |
| Tomcat Hello World             | `gsd-ca-server-client`    | https://gsd-ca-server-client:8443/helloworld/ |               |

## Ansible Software Repository

* [Create a Simple Software Repository for Ansible | C2 Platform](https://c2platform.org/docs/howto/rws/software/)

| Service    | Node                      | Link                                              | Comment                  |
|------------|---------------------------|---------------------------------------------------|--------------------------|
| File Share | `gsd-ansible-file-share1` |                                                   |                          |
| Apache2    | `gsd-ansible-repo`        | https://gsd-ansible-repo.internal.c2platform.org/ | `ansible` `Supersecret!` |
| Windows    | `gsd-ansible-download1`   |                                                   |                          |
| Windows    | `gsd-ansible-download2`   |                                                   |                          |
| Windows    | `gsd-ansible-download3`   |                                                   |                          |

## Tomcat

* [Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and Windows Hosts | C2 Platform](https://c2platform.org/docs/howto/rws/certs/)

| Service                     | Node          | Link                                                         | Reverse Proxy |
|-----------------------------|---------------|--------------------------------------------------------------|---------------|
| Tomcat                      | `gsd-tomcat1` | https://gsd-tomcat1.internal.c2platform.org:8443/helloworld/ |               |
| Tomcat 10 (Windows)         | `gsd-tomcat2` | https://gsd-tomcat2.internal.c2platform.org:8443/helloworld/ |               |
| Tomcat 10 (Windows) Jolokia | `gsd-tomcat2` | https://gsd-tomcat2.internal.c2platform.org:8443/jolokia/    |               |
| Tomcat 10                   | `gsd-tomcat3` | https://gsd-tomcat3.internal.c2platform.org:8443/helloworld/ |               |
| Tomcat 10 Jolokia           | `gsd-tomcat3` | https://gsd-tomcat3.internal.c2platform.org:8443/jolokia/    |               |

## AAP

* [Setup the Automation Controller ( AWX ) using Ansible](https://c2platform.org/docs/howto/rws/aap/awx/)

| Service                           | Node          | Link                  | Reverse Proxy                         |
|-----------------------------------|---------------|-----------------------|---------------------------------------|
| Ansible Controller / AWX          | `gsd-awx1`    | http://1.1.5.17:8052/ | https://awx.c2platform.org/           |
| K8s Dashboard                     | `gsd-awx1`    | https://1.1.5.16      | https://awx-dashboard.c2platform.org/ |
| Ansible Automation Hub / GalaxyNG | `gsd-galaxy1` |                       |                                       |

## CheckMK

* [Setup CheckMK Monitoring using Ansible | C2 Platform](https://c2platform.org/docs/howto/rws/cmk/)

| Service | Node                 | Link                                               | Reverse Proxy                      |
|---------|----------------------|----------------------------------------------------|------------------------------------|
| CheckMK | `gsd-checkmk-server` | http://gsd-checkmk-server:5000/gs/ | https://checkmk.c2platform.org/gs/ |

## Splunk

| Service           | Node         | Link                     | Reverse Proxy                 |
|-------------------|--------------|--------------------------|-------------------------------|
| Splunk            | `gsd-splunk` | http://gsd-splunk:8000/  | https://splunk.c2platform.org |
| Splunk Management | `gsd-splunk` | https://gsd-splunk:8089/ |                               |
