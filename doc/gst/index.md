# RWS GIS Platform Automation Test Environment

## Reverse Proxy

* [Create the Reverse Proxy and Web Proxy | C2 Platform](https://c2platform.org/docs/howto/rws/dev-environment/setup/rproxy1/)

| Service       | Node          | Link                            |
|---------------|---------------|---------------------------------|
| Reverse Proxy | `gst-rproxy1` |                                 |
| Forward Proxy | `gst-rproxy1` |                                 |
| Alive Check   | `gst-rproxy1` | https://c2platform.org/is-alive |

## FME

* [Setting Up FME Flow with Ansible | C2 Platform](https://c2platform.org/docs/howto/rws/arcgis/fme_flow/)

| Service    | Node           | Link                                                            | Reverse Proxy |
|------------|----------------|-----------------------------------------------------------------|---------------|
| AD         | `gst-ad`       |                                                                 |               |
| PostgreSQL | `gst-db1`      |                                                                 |               |
| Tomcat     | `gst-fme-core` | https://gst-fme-core.internal.c2platform.org/                   |               |
| FME        | `gst-fme-core` | https://gst-fme-core.internal.c2platform.org/fmeserver/         |               |
| JSP Test   | `gst-fme-core` | https://gst-fme-core.internal.c2platform.org/helloworld/        |               |
| API Doc    | `gsd-fme-core` | https://gst-fme-core.internal.c2platform.org/fmerest/apidoc/v3/ |               |
