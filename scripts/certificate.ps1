﻿$module = @{}
$module['Params'] = @{}
$module['Result'] = @{}
$module['Result']['msg'] = @{}
$module['Result']['changed']
$module['Result']['certificate']
$module.Params.dns_name = 'gsd-agportal1'
$module.Params.store_location = 'Cert:\LocalMachine\My'
$dns_name = $module.Params.dns_name
$certificates = Get-ChildItem -Path $module.Params.store_location  -Recurse | Where-Object { $_.Subject -eq "CN=$dns_name" }
$certificates_count = $certificates.Count

if ($certificates_count -eq 0) {
    # No certificates found, create a new self-signed certificate in My and then move to store
    $certificate = New-SelfSignedCertificate -DnsName $module.Params.dns_name -CertStoreLocation "Cert:\LocalMachine\My"
    Export-Certificate -Cert $certificate -FilePath "C:\temp\ExportedCertificate.cer"
    $certificate = Import-Certificate -FilePath "C:\temp\ExportedCertificate.cer" -CertStoreLocation $module.Params.store_location
    Remove-Item "C:\temp\ExportedCertificate.cer"
    $module.Result.msg = "Certificate created"
    $module.Result.changed = $true
    Write-Host "Certificate created"
}
else {
    $module.Result.msg = "Certificate found"
    $certificate = $certificates[0]
    $module.Result.changed = $false
    Write-Host "Certificate found"
}
$module.Result.certificate = $certificate

Write-Host "count: $certificates_count"
Write-Host "WHOWHOW: $certificates"