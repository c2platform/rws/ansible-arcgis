import javax.net.ssl.HttpsURLConnection;
import java.net.URL;

public class HttpsConnectionTest {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: java HttpsConnectionTest <URL>");
            System.exit(1);
        }

        String urlString = args[0];

        try {
            URL url = new URL(urlString);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            // Print HTTP response code and message
            System.out.println("Response Code: " + connection.getResponseCode());
            System.out.println("Response Message: " + connection.getResponseMessage());

            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
