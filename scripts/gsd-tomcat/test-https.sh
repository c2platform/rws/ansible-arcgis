keytool -list -cacerts -storepass changeit -alias google
keytool -list -cacerts -storepass changeit -alias c2-ca
javac HttpsConnectionTest.java
java HttpsConnectionTest https://www.google.nl
java HttpsConnectionTest https://c2platform.org
java HttpsConnectionTest https://gsd-tomcat2.apps.c2platform.org:8443/helloworld/
