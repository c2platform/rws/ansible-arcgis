[Net.WebSockets.ClientWebSocket] $ws = [Net.WebSockets.ClientWebSocket]::new()
$uri = [Uri]::new("wss://gsd-fme-core:7078/websocket")

try {
    $ws.ConnectAsync($uri, [Threading.CancellationToken]::None).Wait()

    if ($ws.State -eq [Net.WebSockets.WebSocketState]::Open) {
        Write-Output "Successfully established secure WebSocket connection"
    } else {
        Write-Output "Failed to establish secure WebSocket connection"
    }
} catch {
    Write-Output "Exception occurred: $_"
} finally {
    $ws.Dispose()
}
