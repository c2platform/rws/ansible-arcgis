$UACStatus = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "EnableLUA").EnableLUA

if ($UACStatus -eq 1) {
    Write-Host "UAC is enabled."
} else {
    Write-Host "UAC is disabled."
}