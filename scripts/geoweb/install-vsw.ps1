#Requires -RunAsAdministrator

param (
    [string]$MsiPath =  "C:\vagrant\tmp\VertiGIS-Studio-Web-5.28.1.msi",
    [string]$InstallPath = $(Join-Path ([Environment]::GetFolderPath([System.Environment+SpecialFolder]::ProgramFiles)) "VertiGIS\VertiGIS Studio Web"),
    [string]$ConfigPath = "Setup\VertiGIS.Studio.Web.Setup.exe",
    [string]$VirtualPath = "ModuleViewer",
    [string]$WebSiteId = "1",
    [string]$PortalUrl = "https://age.c2platform.org/portal/home",
    [string]$AppId = "3a01FOjRkQJD2dCv",
    [string]$License = ""
)

$MsiProcess = Start-Process "msiexec.exe" -ArgumentList /i, "`"$(Resolve-Path($MsiPath))`"", "INSTALLDIR=`"$InstallPath`"", /qn, /norestart, /L*, ('{0}.log' -f (get-date -Format yyyyMMddTHHmmss)) -PassThru -Wait -NoNewWindow
if ($MsiProcess.ExitCode -ne 0) {
    Write-Error "msiexec.exe completed with exit code $($MsiProcess.ExitCode)"
    return
}

$PostInstallPath = Join-Path $InstallPath $ConfigPath;
if (!(Test-Path $PostInstallPath)) {
    Write-Error "Post installer not found at $PostInstallPath"
    return
}

if ($License) {
    Write-Host "Set the license $License"
    Start-Process "$PostInstallPath" -ArgumentList /license, /silent, /License:$License -Wait -NoNewWindow
}

Write-Host "Install the license assistant application"
Start-Process "$PostInstallPath" -ArgumentList /licenseassistant, /silent -Wait -NoNewWindow

Write-Host "Create the web application $VirtualPath"
Start-Process "$PostInstallPath" -ArgumentList /deploy, /silent, /Site:$WebSiteId, /SitePath:$VirtualPath, /Storage:$DataPath -Wait -NoNewWindow

if ($AppId) {
    Write-Host "Configure the portal Portal:$PortalUrl, AppID:$AppId"
    Start-Process "$PostInstallPath" -ArgumentList /assign, /silent, /Portal:$PortalUrl, /App:$AppId, /Storage:$DataPath -Wait -NoNewWindow
}

Write-Host "Done"
