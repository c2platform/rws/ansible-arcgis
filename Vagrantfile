# frozen_string_literal: true

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'openssl'
require 'yaml'
require 'net/http'

nodes_info = YAML.load_file('Vagrantfile.yml')

Vagrant.configure(2) do |config|
  config.vm.synced_folder '.', '/vagrant'
  sync_folders_additional(config.vm, nodes_info['sync_folders'], '.sync_folders.yml')
  nodes_info['nodes'].each do |nd|
    next if nd['disabled']
    nd_name = "#{box_prefix(nd, nodes_info)}-#{nd['name']}" # .e.g. gsd-proxy1
    bx = nodes_info['boxes'][nd['box']] || nodes_info['boxes']['default']
    bx_url = box_url(bx['name'], bx['version'])
    config.vm.define nd_name do |cfg|
      cfg.vm.hostname = nd_name
      cfg.vm.box = bx['name']
      cfg.vm.box_version = bx['version']
      if bx_url
        cfg.vm.box_url = bx_url
        cfg.vm.box_version = bx['version']
      end
      sync_folders_additional(cfg.vm, nd['sync_folders'])
      if bx['registration'] || false
        if ENV['RHEL_DEV_ACCOUNT'].nil? || ENV['RHEL_DEV_SECRET'].nil?
          abort 'ERROR: Please set both RHEL_DEV_ACCOUNT and RHEL_DEV_SECRET ' \
          'environment variables. Refer to ' \
          'https://c2platform.org/docs/guidelines/dev/rhel for more info.'
        end
        cfg.registration.skip = false
        cfg.registration.unregister_on_halt = false
        cfg.registration.username = ENV['RHEL_DEV_ACCOUNT']
        cfg.registration.password = ENV['RHEL_DEV_SECRET']
      end
      if bx['provider'] == 'lxd'
        cfg.ssh.insert_key = false
        cfg.vm.provider :lxd do |lxd|  # configure lxd provider
          # lxd.container_name = nd_name
          # lxd.api_endpoint = 'https://127.0.0.1:8443'
          lxd.timeout = 30
          lxd.name = nd_name
          lxd.nesting = nil
          lxd.privileged = true
          lxd.ephemeral = false
          lxd.profiles = ['default', 'microk8s']
          lxd.environment = {}
          lxd.config = {
            'boot.autostart': 'false'
          }
          if nd['ip-address']
            lxd.devices = {
              eth1: {
                'ipv4.address': nd['ip-address'],
                nictype: 'bridged',
                parent: 'lxdbrag1',
                type: 'nic'
              }
            }
          end
        end
      else # virtualbox
        if nd['ip-address']
          nd['ip-address'].split(',').each do |ip|
            cfg.vm.network 'private_network', ip: ip
          end
        end
        cfg.vm.network 'public_network', bridge: nodes_info['defaults']['bridge'], mac: nd['mac'] if nd['mac']
        if nd['vm-communicator'] || bx['vm-communicator'] == 'winrm'
          cfg.vm.communicator = 'winrm'
          cfg.winrm.username = 'vagrant'
          cfg.winrm.password = 'vagrant'
          cfg.winrm.basic_auth_only = true
        else
          cfg.ssh.insert_key = false
          #cfg.ssh.username = 'vagrant'
          #cfg.ssh.password = 'vagrant'
          #cfg.ssh.keys_only = false
        end
        cfg.vm.provider 'virtualbox' do |vb|  # configure virtualbox provider
          vb.gui = false
          vb.memory = nd['memory'] || 1024
          vb.cpus = nd['cpus'] || 2
          add_disk(vb, 1, 300 * 1024, "#{nd['name']}.vdi", (nd['disk-controller'] || 'IDE Controller')) if nd['data-disk'] == true
          unless nd['trace'].nil?
            trace(vb,nodes_info['defaults']['trace-dir'],:id, nd_name, nd['trace'] )
          end
          # vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
        end
      end
      cfg.vm.provision 'windows-sysprep' if nd['sysprep'] or bx['sysprep']
      cfg.vm.provision 'shell', inline: (nd['shell-provision'] || bx['shell-provision'] || nodes_info['defaults']['shell-provision'])
      plays = ENV['PLAY'] ? [ENV['PLAY']] : nd['plays']
      mnp = multinode_provision(plays)
      if !mnp || (nd_name == last_node)
        plays.each do |p|
          cfg.vm.provision 'ansible' do |ansible|  # configure ansible provisioner
            ansible.config_file = 'ansible-dev.cfg'
            ansible.playbook = ansible_play(p)
            ansible.compatibility_mode = '2.0'
            ansible.inventory_path = 'hosts.ini'
            if p.include? 'restart' or p.include? 'start' or p.include? 'stop'  # restart play is orchestrated from localhost
              ansible.limit = 'localhost'
            else
              ansible.limit = limit if mnp
            end
            # ansible.galaxy_role_file = '../roles/requirements.yml'
            # ansible.galaxy_roles_path = '~/.ansible/roles'
            # ansible.verbose = 'vvv'
            ansible.tags = ENV['TAGS'] if ENV['TAGS']
            ansible.skip_tags = ENV['SKIP_TAGS'] if ENV['SKIP_TAGS']
          end
        end
      end
    end
  end
end

def trace(vb_provider, trace_dir, nd_id, nd_name, on_off = 'on', nic_no = 2)
  #puts "on_off: #{on_off.inspect}"
  vb_provider.customize ['modifyvm', nd_id, "--nictrace#{nic_no}", on_off, "--nictracefile#{nic_no}", "#{ trace_dir}/#{nd_name}.pcap"]
  puts "Trace #{nd_name} #{ trace_dir}/#{nd_name}.pcap" if on_off == "on"
end

def add_disk(vb_provider, port = 2, size = 120 * 1024, filename = nil, controller = 'IDE Controller')
  filename = "disk#{port}.vdi" if filename.nil?
  disk = File.expand_path(filename)
  vb_provider.customize ['createhd', '--filename', disk, '--format', 'VDI', '--size', size] unless File.exist?(disk)
  vb_provider.customize ['storageattach', :id, '--storagectl', controller, '--port', port, '--device', 0, '--type', 'hdd', '--medium', disk]
end

def vm_exists?(vm_name = 'default', provider = 'virtualbox')
  File.exist?(File.join(File.dirname(__FILE__), ".vagrant/machines/#{vm_name}/#{provider}/vagrant_cwd"))
end

def box_prefix(nd, nd_info)
  nd['prefix'] || nd_info['defaults']['prefix']
end

def box_url(box_name, box_version)
  nodes_info = YAML.load_file('Vagrantfile.yml')
  if nodes_info['defaults']['repo']
    url = File.join(nodes_info['defaults']['repo'], "#{box_name}-#{box_version}.json")
    unless url.nil?
      res = Net::HTTP.get_response(URI.parse(url.to_s))
      url unless res.code == '404'
    end
  end
end

def limit
  if nodes_selected == []
    'all'
  else
    nodes_selected.join(':')
  end
end

def ansible_play(play)
  ps = ["./plays/#{play}.yml", "../ansible/plays/#{play}.yml", play]
  ps2 = ps.collect { |pth| pth if File.exist?(pth) }.compact
  if ps2.empty?
    raise "Play #{play} not found! Paths #{ps.join(', ')} don't exists"
  else
    ps2.first
  end
end

# Return last node in command or the default multiprovison node
def last_node
  ns = nodes_selected
  if ns == []
    nodes_info = YAML.load_file('Vagrantfile.yml')
    nodes_info['defaults']['node_multinode_provision']
  else
    ns.last
  end
end

def nodes_selected
  nodes_info = YAML.load_file('Vagrantfile.yml')
  nodes_all = nodes_info['nodes'].collect { |n| "#{box_prefix(n, nodes_info)}-#{n['name']}" }
  nodes_selected = []
  ARGV.each do |a|
    nodes_selected << a if nodes_all.include?(a)
  end
  nodes_selected
end

# Return true if the play contains "multinode" or targets multiple hosts
def multinode_provision(plays)
  return false unless plays
  plays.each do |play|
    play_yml = File.read(ansible_play(play))

    # Check if "multinode" appears in the play
    return true if play_yml.include?("multi-node")

    # Count occurrences of import_playbook and hosts to determine if multiple hosts are targeted
    cnt = play_yml.scan(/import_playbook:\s?(\S+)/m).count +
      play_yml.scan(/hosts:\s?(\S+)/m).count
    return true if cnt > 1
    return true if ENV['PROVISION_LOCALHOST']  # Check if the environment variable PROVISION_LOCALHOST is set
  end
  return false
end

def sync_folders_additional(config_vm, sync_folders, sync_folders_yml_path = nil)
  fldrs = []
  if sync_folders_yml_path
    if File.exists?(sync_folders_yml_path)
      fldrs = YAML.load_file(sync_folders_yml_path)
    end
  end
  fldrs += sync_folders if sync_folders
  fldrs.each do |f|
    src_path = File.expand_path(f['src'])
    if f['owner']
      config_vm.synced_folder src_path, f['target'],
      owner: f['owner'], group: f['group']
    else
      config_vm.synced_folder src_path, f['target']
    end
  end
end

# vpass file for Ansible vault secrets.yml
vpass_file = File.join(File.dirname(__FILE__), 'vpass')
File.open(vpass_file, 'w') { |f| f.write('secret') } unless File.exist? vpass_file
