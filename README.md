# RWS GIS Platform Reference Implementation

[![pipeline status](https://gitlab.com/c2platform/rws/ansible-gis/badges/master/pipeline.svg)](https://gitlab.com/c2platform/rws/ansible-gis/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/rws/ansible-gis/-/badges/release.svg)](https://gitlab.com/c2platform/rws/ansible-gis/-/releases)

The RWS GIS Platform Reference Implementation is hosted on GitLab and functions as the central
[Ansible Reference Implementation Project](/docs/concepts/ansible/projects/refimpl)
dedicated to supporting the Rijkswaterstaat (RWS) GIS Platform.

- [Purpose](#purpose)
- [Overview](#overview)
- [Plays](#plays)
  - [Ansible Software Repository](#ansible-software-repository)
- [How-to](#how-to)
- [Variables](#variables)
  - [`gs_envs`: Environment Group Validation](#gs_envs-environment-group-validation)
  - [`gs_tomcat_https_port`](#gs_tomcat_https_port)
  - [`gs_download_temp_dir`](#gs_download_temp_dir)


## Purpose

This project has two primary objectives:

1. **Supporting RWS Development Environment:** The RWS GIS Platform Ansible
   Project is an integral part of the RWS development environment, serving as a
   cornerstone for the RWS Ansible collections. It provides a structured
   inventory and automation framework that streamlines the development and
   management of various Ansible-based tasks and workflows within the RWS GIS
   Platform.
1. **Open-Source Reference Implementation:** Beyond its role within RWS, this
   project also operates as an open-source reference implementation. It offers a
   valuable resource for showcasing diverse automation solutions and
   exemplifying industry best practices. By doing so, it encourages
   collaboration and knowledge sharing among the wider automation community.

In summary, the RWS GIS Platform Ansible Project plays a pivotal role in
enhancing the efficiency of RWS operations while contributing to the broader
open-source ecosystem by serving as a model for effective automation practices.


## Overview

[RWS GIS Platform](https://c2platform.org/docs/projects/rws)

## Plays

### Ansible Software Repository

This play targets three group of which only `ansible_repo` is relevant for RWS
environments because the File Share is an external system and the Download group
was only created to verify the setup.

| Group                | Purpose                            | Comment | RWS |
|----------------------|------------------------------------|---------|-----|
| `ansible_file_share` | The Windows File Share             |         |     |
| `ansible_repo`       | The Apache web server              |         | ✔   |
| `ansible_download`   | Windows that perform test download |         |     |

A key variable for the Ansible Software Repository play
`plays/mgmt/ansible_repo.yml` targets is `gs_software_repository_ansible`:

| Parameter              | Description |
|------------------------|-------------|
| `server`               |             |
| `url`                  |             |
| `unc`                  |             |
| `unc-win`              |             |
| `path`                 |             |
| `username`             |             |
| `password`             |             |
| `cache`                |             |
| `apache_username`      |             |
| `apache_password`      |             |
| `apache_htpasswd_path` |             |
| `etc_fstab_cred_file`  |             |

```yaml
gs_software_repository_ansible:
  server: "{{ groups['ansible_file_share'][0] }}.internal.c2platform.org"
  url: "https://{{ groups['ansible_repo'][0] }}.internal.c2platform.org/software"
  unc: "//{{ groups['ansible_file_share'][0] }}.internal.c2platform.org/ansible-repo"
  unc-win: '\\{{ groups["ansible_file_share"][0] }}.internal.c2platform.org\ansible-repo'
  path: D:/file-share
  username: svc_ansible_repository
  password: "{{ gs_software_repository_ansible_password }}"  # vault
  cache: "//{{ groups['ansible_file_share'][0] }}.internal.c2platform.org/ansible-repo/cache"
  apache_username: ansible
  apache_password: "{{ gs_software_repository_ansible_apache_password }}"  # vault
  apache_htpasswd_path: /etc/httpd/.htpasswd
  etc_fstab_cred_file: "/etc/cred.svc_ansible_repository"
  # drive: S  # not used
  # cache: S:/cache
```

Refer to
[Create a Simple Software Repository for Ansible](https://c2platform.org/docs/howto/rws/software/)
for more information.

## How-to

Step-by-step instructions on what you can do with this project are published on
C2 Platform website, see
[How-to Rijkswaterstaat | C2 Platform](https://c2platform.org/en/docs/howto/rws/)

## Variables

This section provides documentation for project-specific variables utilizing the
`gs_` prefix, which are introduced and employed within this inventory project.
To better understand the conventions for variable prefixes, you can refer to the
[Variable prefix | C2 Platform](https://c2platform.org/en/docs/guidelines/coding/var-prefix/)
documentation.

### `gs_envs`: Environment Group Validation

This Ansible inventory project employs a group-based approach to organize and
manage different DTAP environments. The `gs_envs` variable is a list that
specifies the target environments for this project, such as `['development',
'test', 'acceptance', 'production']`

The `gs_envs` variable is used to validate that each targeted node is associated
with exactly one environment group. This validation check is implemented in the
`group_vars/windows/main.yml` and `group_vars/linux/main.yml` files.

For more information on the group-based environment approach, refer to our
documentation:
[Group-based environments | C2 Platform](https://c2platform.org/docs/guidelines/setup/group-based-environments/).

### `gs_tomcat_https_port`

Used to configure **FME Flow** and **Tomcat**:

1. Run the FME Installer.
2. Configure Tomcat
3. Manage `fmeServerConfig.txt`

### `gs_download_temp_dir`

In this project, several roles utilized within the plays rely on the
`c2platform.wincore.download`. One key variable, `gs_download_temp_dir`, is defined
in `group_vars/all/main.yml`, as illustrated below:

```yaml
gs_download_temp_dir:
  path: C:\vagrant\tmp
  # create: yes
  # delete: no
  # recursive: true
  delete_files: false
```
This variable is declared at the global level (in `all.yml`) to ensure that its
settings can be conveniently reused throughout the inventory project. As an
example, within `group_vars/fme/main.yml`, you can find the following line:

```yaml
fme_flow_temp_dir: "{{ gs_download_temp_dir }}"
```
